package Dao;

import java.beans.IntrospectionException;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectionFactory;





/**
 * Aceasta clasa utilizeaza  efectueaza
 * operatiile de SELECT *, SELECT, INSERT, DELETE si UPDATE asupra tabelelor din baza de date
 * 
 * 
 * @author user
 *
 * @param <T>
 *            clasa ce reprezinta modelul tabelului din BD asupra caruia se
 *            efectueaza operatii
 */


public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	

	
	/**
	 * 
	 * @return un string ce creeaza interogarea pentru gasirea tuturor obiectelor
	 */
	
	
	private String createSelectAllQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		return sb.toString();
	}
	
	
	
	
	/**
	 * @param field reprezinta campul dupa care se face selectia in DB
	 * @return un string ce creeaza o interogarea
	 */
	
	
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	
	
	/**
	 * @param field reprezinta campul dupa care se face stergerea in DB
	 * @return un string ce creeaza o interogarea
	 */
	

	private String createDeleteQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}
	

	
	/**
	 * @param field reprezinta coloanele tabelului in care se face inserarea in DB
	 * @return un string ce creeaza o interogarea
	 */
	
	
	private String createInsertQuery(ArrayList<String> field) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT ");
		sb.append("INTO ");
		sb.append(type.getSimpleName());
		sb.append(" (");
		for(int i=0; i<field.size();i++)
		{
			if(i<field.size()-1) 
				sb.append(field.get(i) + ", ");
			else
				sb.append(field.get(i));
		}
		sb.append(")");
		
		sb.append(" VALUES ");
		
		sb.append("(");
		for(int i=0;i<field.size();i++)
		{
			if(i<field.size()-1) 
				sb.append("?" + ",");
			else
				sb.append("?");
		}
		sb.append(")");
	
		return sb.toString();
	}
	

	
	
	/**
	 * @param field reprezinta coloanele tabelului in care se face update in DB
	 * @return un string ce creeaza o interogarea
	 */
	
	
	private String createUpdatetQuery(ArrayList<String> field) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName());
		sb.append("SET ");
		
		for(int i=0;i<field.size();i++) {
			if(i<field.size()-1) 
				sb.append(field.get(i) + "=" + "?" + ",");
				else
					sb.append(field.get(i) + "=" + "?");
			
		}
		sb.append("WHERE id =?");
	
		return sb.toString();
	
	}
	
	/**
	 * 
	 * @return toate inregistrarile tabelului
	 */
	
	public List<T> findAll() {
		// TODO:
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		return null;
	}

	
	
	/**
	 * @param id reprezinta id-ul dupa care se cauta intr un tabel
	 * @return null in cazul in care nu se gaseste id-ul respectiv
	 */
	
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	
	
	/**
	 * @param name reprezinta numele dupa care se cauta intr un tabel
	 * @return null in cazul in care nu se gaseste id-ul respectiv
	 */
	
	public T findByName(String name) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("name");
		try {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1, name);
			resultSet = statement.executeQuery();
			
			if(resultSet.next())
				return createObjects(resultSet).get(0);
			else
				return null;

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	
	
	
	
	
	/**
	 * @param resultSet
	 *  parametru de tip resultSet, care este setul de rezultate dupa
	 *            instructiunea SQL executata
	 * 
	 * @return o lista de obiecte care coincide cu rezultatul instructiunii SQL
	 */
		

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	
		
	
	/**
	 * @param id reprezinta id-ul inregistrarii care va fi stearsa
	 * @return false in cazul in care nu se gaseste id-ul respectiv si true in caz contrar
	 */
	
	public boolean deleteById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createDeleteQuery("id");
		boolean ok=true;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:deleteById " + e.getMessage());
			ok=false;
			
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return ok;
	}
	
	
	
	
	/**
	 * @param name reprezinta numele inregistrarii care va fi stearsa
	 * @return false in cazul in care nu se gaseste id-ul respectiv si true in caz contrar
	 */
	
	
	public boolean deleteByName(String name) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createDeleteQuery("name");
		boolean ok=true;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1, name);
			statement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:deleteByName " + e.getMessage());
			ok=false;
			
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return ok;
	}
	
	
	
	
	
	/**
	 * 
	 * @param t
	 *            un obiect care se doreste adaugat in tabel
	 * @return obiectul in sine daca s-a adaugat, null altfel
	 */
	
		
	
	public T insert(T t) {
		ReflectionOperator list = new ReflectionOperator();
		list.retrieveProperties(t);
		ArrayList<String>listF=list.getListField() ;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createInsertQuery(listF);
		

		boolean ok=true;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			
			ArrayList<Object>listV=list.getListValue();
		     
          
           int i=1;
			for(Object value:listV) {
				
				System.out.println(value);
				if(value.getClass().getSimpleName().equals("Integer")) {
					statement.setInt(i, (int) value);
					
				}
				
				if(value.getClass().getSimpleName().equals("String")) {
					statement.setString(i, (String) value);	
					//System.out.println("inside2");

				}
				
				if(value.getClass().getSimpleName().equals("Float")) {
					statement.setFloat(i, (float) value);	

				}
				
				i++;
			}
			
			System.out.println(statement.toString());
			statement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
			ok=false;
			
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		
		if(ok==true)
		return t;
		else 
			return null;

		
	}

	public T update(int id,T t) {
		// TODO:
		
		T upd = findById(id);
		if(upd==null)  //daca obiectul nu a fost gasit nu ii putem face update
			return null;
		
		ReflectionOperator list = new ReflectionOperator();
		ArrayList<String>listF=list.getListField() ;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createUpdatetQuery(listF);
		boolean ok=true;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			
			ArrayList<Object>listV=list.getListValue();
			for(Object value:listV) {
				int i=1;
				
				if(value.getClass().getName().equals("Int")) {
					statement.setInt(i, (int) value);		
				}
				
				if(value.getClass().getName().equals("String")) {
					statement.setString(i, (String) value);		
				}
				
				if(value.getClass().getName().equals("Float")) {
					statement.setFloat(i, (float) value);		
				}
				
				i++;
			}

			statement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
			ok=false;
			
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		if(ok==true)
		return upd;
		else 
			return null;
		
	
	}
}

