package Bll;

import java.util.ArrayList;



import java.util.List;
import java.util.NoSuchElementException;

import Bll.validator.OrderClientValidator;
import Bll.validator.OrderProductValidator;
import Bll.validator.OrderQuantityValidator;
import Bll.validator.Validator;
import Dao.OrderDAO;
import Model.Client;
import Model.Orders;
import Model.Product;



/**
 * Clasa Business pentru comenzi
 * 
 * @author user
 *
 */

public class OrderBLL {

	private List<Validator<Orders>> validators;
	private OrderDAO orderDAO;
	
		
	
	public List<Validator<Orders>> getValidators() {
		return validators;
	}


	public void setValidators(List<Validator<Orders>> validators) {
		this.validators = validators;
	}




	public OrderDAO getOrderDAO() {
		return orderDAO;
	}


	public void setOrderDAO(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}




	public OrderBLL() {
		validators = new ArrayList<Validator<Orders>>();
		validators.add(new OrderClientValidator());
		validators.add(new OrderProductValidator());
		validators.add(new OrderQuantityValidator());

		orderDAO = new OrderDAO();
	}
	
		
	

	public Orders findOrderById(int id) {
		Orders ord = orderDAO.findById(id);
		if (ord == null) {
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		}
		return ord;
	}
	
	
	
	public void insert(Orders order) {
		Orders ord= orderDAO.insert(order);
		if (ord == null) {
			throw new NoSuchElementException("The order cannot be inserted!");
		}
	}
	
	
	
	public void insertOrder(String nameClient,String nameProduct,int quantity) {
		
		GeneratePDF pdf =new GeneratePDF();

		ClientBLL clientBLL = new ClientBLL();	
		Client client = clientBLL.findClientByName(nameClient);
		int i=1;
         if(client!=null)
         {
     		ProductBLL productBLL = new ProductBLL();	
    		Product product = productBLL.findProductByName(nameProduct);
    		if(product!=null)
    		{
    			if(quantity<=product.getInitialQuantity())
    			{
    				Orders order=new Orders(client.getId(),product.getId(),product.getInitialQuantity());
    				orderDAO.insert(order);
    				
					 pdf.generateBillForOrder(i,nameClient,product.getPrice(),nameProduct,quantity);
					 i++;
    			}
    			else
    			{
                   pdf.generateUnderStockMessage(nameClient,quantity,product.getInitialQuantity());    				
    			}
    		}
         }
	
	}
	
		
	
}

