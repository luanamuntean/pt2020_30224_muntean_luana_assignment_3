package Bll;
import java.io.FileNotFoundException;


import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


import Model.Client;
import Model.Orders;
import Model.Product;



/**
 * Clasa Business pentru generarea fisierelor PDF
 * 
 * @author user
 *
 */

public class GeneratePDF {
	
	
	private void addClientHeader(PdfPTable table) {
		Stream.of("id", "name", "town" ,"email","age").forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
	}
	

	public void generateClientReport(List<Client> clients) {
		Document document = new Document();


		try {
			PdfWriter.getInstance(document, new FileOutputStream("ClientsPDFClient.pdf"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		document.open();

		PdfPTable table = new PdfPTable(5);
		addClientHeader(table);
		// add customers in tabular format
		for (Client c : clients) {
			table.addCell(Integer.toString(c.getId()));
			table.addCell(c.getName());
			table.addCell(c.getTown());
			table.addCell(c.getEmail());
			table.addCell(Integer.toString(c.getAge()));


		}

		try {
			document.add(table);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

	}
	
	
	
	
	
	public void generateProductsReport(List<Product> products) {
		Document document = new Document();

		try {
			PdfWriter.getInstance(document, new FileOutputStream("ProductPDF.pdf"));
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		document.open();

		PdfPTable table = new PdfPTable(4);
		addProductHeader(table);
		// add customers in tabular format
		for (Product p : products) {
			table.addCell(Integer.toString(p.getId()));
			table.addCell(p.getName());
			table.addCell(Integer.toString(p.getInitialQuantity()));
			table.addCell(Float.toString(p.getPrice()));

		}

		try {
			document.add(table);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
	}

         
	
	


	private void addProductHeader(PdfPTable table) {
		Stream.of("id", "name", "initialQuantity", "price").forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
	}
	
	
	
	
	
	
	public void generateOrders(List<Orders> orders) {
		Document document = new Document();

		try {
			PdfWriter.getInstance(document, new FileOutputStream("OrderPDF.pdf"));
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		document.open();

		PdfPTable table = new PdfPTable(4);
		addOrderHeader(table);
		// add customers in tabular format
		for (Orders ord : orders) {
			table.addCell(Integer.toString(ord.getId()));
			table.addCell(Integer.toString(ord.getIdClient()));
			table.addCell(Integer.toString(ord.getIdProduct()));
			table.addCell(Integer.toString(ord.getQuantity()));

		}

		try {
			document.add(table);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
	}
	
	
	
	
	private void addOrderHeader(PdfPTable table) {
		Stream.of("id", "idClient", "idProduct", "quantity").forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
	}
	
	
	
	
	public void generateBillForOrder(int billNo, String clientName, float price, String prodName, int quantity){
        Document document = new Document();

        try {
            PdfWriter.getInstance(document, new FileOutputStream("Bill"+billNo+".pdf"));
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLUE);
//            Chunk chunk = new Chunk("Invoice", font);

            PdfPTable table = new PdfPTable(5);
            List<String> colNames = new ArrayList<>();
            colNames.add("Client");
            colNames.add("Product");
            colNames.add("Price/Unit");
            colNames.add("Qty");
            colNames.add("Total");
            addTableHeader(table, colNames);

            table.addCell(clientName);
            table.addCell(prodName);
            table.addCell(String.valueOf(price)+" $");
            table.addCell(String.valueOf(quantity));
            table.addCell(String.valueOf(quantity*price)+" $");


//            document.add(chunk);
            document.add(table);

            document.close();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch(Exception d){
            d.printStackTrace();
        }
    }
	
	
	
	
	private static void addTableHeader(PdfPTable table, List<String> colNames) {
        colNames.stream()
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(1);
                    header.setPhrase(new Phrase(String.valueOf(columnTitle)));
                    table.addCell(header);
                });
    }
	
	
	
	
	public void generateUnderStockMessage(String prodName, int requestedQty, int availableQty){
        Document document = new Document();

        try {
            PdfWriter.getInstance(document, new FileOutputStream("UnderStockMessage.pdf"));
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.RED);
            Font font2 = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.ORANGE);
            Chunk chunk = new Chunk("Not enough "+prodName+"s available in stock !", font);
            Chunk chunk2 = new Chunk(" Requested: "+requestedQty+"; Available: "+availableQty, font2);

            Paragraph paragraph = new Paragraph(chunk);
            Paragraph paragraph2 = new Paragraph(chunk2);

            document.add(paragraph);
            document.add(Chunk.NEWLINE);
            document.add(paragraph2);

            document.close();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch(Exception d){
            d.printStackTrace();
        }
    }
	
	
}

	
	
	
	
	
	
