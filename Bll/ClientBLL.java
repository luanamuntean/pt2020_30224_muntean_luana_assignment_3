package Bll;

import java.util.ArrayList;

/**
 * Clasa Business pentru clienti
 * 
 * @author user
 *
 */

import java.util.List;
import java.util.NoSuchElementException;

import Bll.validator.ClientAgeValidator;
import Bll.validator.ClientEmailValidator;
import Bll.validator.Validator;
import Dao.ClientDAO;
import Model.Client;

public class ClientBLL {

	private List<Validator<Client>> validators;
	private ClientDAO clientDAO;
	
	
	
	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new ClientEmailValidator());
		validators.add(new ClientAgeValidator());

		clientDAO = new ClientDAO();
	}
	
	

	public List<Validator<Client>> getValidators() {
		return validators;
	}

	public void setValidators(List<Validator<Client>> validators) {
		this.validators = validators;
	}
	
	

	
	public ClientDAO getClientDAO() {
		return clientDAO;
	}

	public void setClientDAO(ClientDAO clientDAO) {
		this.clientDAO = clientDAO;
	}
	

	
	public List<Client> findAllClients() {
		List<Client> list = clientDAO.findAll();
		if(list==null) {
			throw new NoSuchElementException("Error:Doesn't exist clients in db");

		}
		return list;
	}
	
	

	public Client findClientById(int id) {
		Client cl = clientDAO.findById(id);
		if (cl == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return cl;
	}
	
	
	
	public Client findClientByName(String name) {
		Client cl = clientDAO.findByName(name);
		if (cl == null) {
			throw new NoSuchElementException("The client with name =" + name + " was not found!");
		}
		return cl;
	}
	
	
	public void insertClientById(Client client) {
		Client cl = clientDAO.insert(client);
		if (cl == null) {
			throw new NoSuchElementException("The client cannot be inserted");
		}

	}
	
	public boolean deleteClientById(int id) {
		Client cl = clientDAO.findById(id);
		if (cl == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		
		return clientDAO.deleteById(id);

	}
	
	
	
	public boolean deleteClientByName(String name) {
		Client cl = clientDAO.findByName(name);
		if (cl == null) {
			throw new NoSuchElementException("The client with name =" + name + " was not found!");
		}
		
		return clientDAO.deleteByName(name);

	}
	
	
	public Client updateClientById(int id,Client client) {
		Client cl = clientDAO.update(id,client);
		if (cl == null) {
			throw new NoSuchElementException("The client was not found!");
		}
		return cl;
	}
	
	
	
}
