package Bll;

import java.util.ArrayList;



import java.util.List;
import java.util.NoSuchElementException;

import Bll.validator.ProductPriceValidator;
import Bll.validator.ProductQuantityValidator;
import Bll.validator.Validator;
import Dao.ProductDAO;

import Model.Product;


/**
 * Clasa Business pentru produse
 * 
 * @author user
 *
 */

public class ProductBLL {

	private List<Validator<Product>> validators;
	private ProductDAO productDAO;

	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
		validators.add(new ProductPriceValidator());
		validators.add(new ProductQuantityValidator());

		productDAO = new ProductDAO();
	}
	
	


	public List<Validator<Product>> getValidators() {
		return validators;
	}


	public void setValidators(List<Validator<Product>> validators) {
		this.validators = validators;
	}





	public ProductDAO getProductDAO() {
		return productDAO;
	}

	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}


	
	
	public List<Product> findAllProducts() {
		List<Product> list = productDAO.findAll();
		if(list==null) {
			throw new NoSuchElementException("Error:Doesn't exist products in db");

		}
		return list;
	}

	
	public Product findProductByName(String name){
        Product prod = productDAO.findByName(name);

        if(prod == null)
            throw new NoSuchElementException("The product named '"+name+"' doesn't exist!");
        return prod;
    }
	
	


	public Product findProductById(int id) {
		Product prod = productDAO.findById(id);
		if (prod == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return prod;
	}
	
	
	
	
	public void insert(Product product) {
		Product prod = productDAO.insert(product);
		if (prod == null) {
			throw new NoSuchElementException("The product cannot be inserted!");
		}
	}
	
	
	public boolean deleteProductById(int id) {
		Product prod = productDAO.findById(id);
	
		if (prod == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		else
	
		return deleteProductById(id);
	}
	
	
	
	public boolean deleteProductByName(String name) {
		Product prod = productDAO.findByName(name);
		if (prod == null) {
			throw new NoSuchElementException("The product with name =" + name + " was not found!");
		}
		
		return productDAO.deleteByName(name);

	}
	
	
	
	public Product updateProductById(int id,Product product) {
		Product prod = productDAO.update(id,product);
		if (prod == null) {
			throw new NoSuchElementException("The product was not found!");
		}
		return prod;
	}
	
	

}

