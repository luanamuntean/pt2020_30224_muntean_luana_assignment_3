package Bll.validator;
import Bll.ClientBLL;

import Model.Client;

import Model.Orders;


/**
 * Aceasta clasa verifica daca clientul care plaseaza comanda exista sau nu in baza de date 
 * 
 * @author user
 */

public class OrderClientValidator implements Validator<Orders> {

	public void validate(Orders t) {
		// TODO Auto-generated method stub
		ClientBLL clientBLL = new ClientBLL();	
			Client client = clientBLL.findClientById(t.getIdClient());
	         if(client==null) {
					throw new IllegalArgumentException("Client with id = " + t.getIdClient() + " was not found!");

	         }
	         else
	         {
					System.out.println("Order taken!");

	         }
		
	}

}
