package Bll.validator;

import Model.Product;


/**
 * Aceasta clasa verifica daca cantitatea produsului din baza de data este pozitiva sau negativa 
 * 
 * @author user
 */

public class ProductQuantityValidator implements Validator<Product> {
	public void validate(Product t) {

		if (t.getInitialQuantity()<0) {
			throw new IllegalArgumentException("Error:negative quantity");
		}

	}

}
