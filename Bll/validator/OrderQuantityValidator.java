package Bll.validator;

import Bll.ProductBLL;

import Model.Orders;
import Model.Product;

/**
 * Aceasta clasa verifica daca cantitatea dorita de client este disponibila in baza de date 
 *  
 * @author user
 */

public class OrderQuantityValidator implements  Validator<Orders> {

	@Override
	public void validate(Orders t) {
		// TODO Auto-generated method stub
		if (t.getQuantity()<0) {
			throw new IllegalArgumentException("Error:negative quantity");
		}
		else
		{
			ProductBLL productBLL = new ProductBLL();
			Product product = productBLL.findProductById(t.getIdProduct());
			if(product==null) {
				throw new IllegalArgumentException("Product with id = " + t.getIdProduct() + " was not found!");
			}
			else
			{
				if(t.getQuantity()>product.getInitialQuantity()) {
					throw new IllegalArgumentException("Error:doesn't exist this quantity");

				}
				else {
					System.out.println("Order taken!");
				}
			}
			
		}
		
	}

}
