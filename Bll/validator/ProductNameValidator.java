package Bll.validator;

import Bll.ProductBLL;


import Model.Product;

/**
 * Aceasta clasa verifica daca numele produsului se afla sau nu in baza de date
 *  
 * @author user
 */

public class ProductNameValidator implements Validator<Product>{

	
	public void validate(Product t) {
		// TODO Auto-generated method stub
		ProductBLL productBLL = new ProductBLL();	
		  Product product = productBLL.findProductByName(t.getName());
		  
		  
	         if(product!=null) {
					throw new IllegalArgumentException("Error:This product exists in db");

	         }
	        
		
	}
	
}
