import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import Bll.ClientBLL;
import Bll.OrderBLL;
import Bll.ProductBLL;
import Model.Client;
import Model.Orders;
import Model.Product;



/**
 * Clasa Parser reprezinta meniu principal
 * 
 * @author user
 *
 */


public class Parser {


	 private String nameClient;
	 private String townClient;
	
	 private String nameProduct;
	 private int quantityProduct;
	 private float priceProduct;
	 
//	 private String text;

	 
	 
	 
	 public  void citesteDate(String fileName) throws SQLException, IOException{
		
			

		   
			FileInputStream f=new FileInputStream(fileName);
			InputStreamReader fchar=new InputStreamReader(f);
			BufferedReader buf=new BufferedReader(fchar);
			 String citeste=buf.readLine();
			
			
			while((citeste!=null) &&(!citeste.isEmpty()))
			{
                 				

				
				String [] rez =null;
			
				if(citeste.matches("(.*):(.*)")) {
					  
					 rez =citeste.split(":");
					 
					 if(rez[0].equals("Insert client")) {
						 String [] detailsClient;
						 detailsClient=rez[1].split(", ");
						 nameClient=detailsClient[0];
						 townClient=detailsClient[1];
						 
						 //function of insert client of DB
						    ClientBLL clientBLL = new ClientBLL();
						       
							Client client1 = new Client(nameClient,townClient,"userx@yahoo.com",35);

					        try {
					           clientBLL.insertClientById(client1);
					        } catch (Exception e){
					           // LOGGER.log(Level.INFO, e.getMessage());

					        }


					        if(client1!=null){
					            System.out.println(client1.toString());
					       }
						 

						 
					 }
					 
					 else
					 {
						 if(rez[0].equals("Delete client")) {
							 String [] detailsClient;
							 detailsClient=rez[1].split(", ");
							 nameClient=detailsClient[0];
							 townClient=detailsClient[1];
							 //function of delete client of DB
							 
							 ClientBLL clientBLL = new ClientBLL();
						       
								boolean ok=false;

						        try {
						          ok= clientBLL.deleteClientByName(nameClient);
						        } catch (Exception e){
						        	
						        }
							
							
							 
						 }
					 
					 
					 else
					 {
						 if(rez[0].equals("Insert product")) {
							 String [] detailsProduct;
							 detailsProduct=rez[1].split(", ");
							 nameProduct=detailsProduct[0];
							 quantityProduct=Integer.parseInt(detailsProduct[1]);
							 priceProduct=Float.parseFloat(detailsProduct[2]);
							 //System.out.println(rez[0] + nameProduct+ " " + quantityProduct + " " + priceProduct);
							 //function of insert product of DB
							 
							  ProductBLL clientBLL = new ProductBLL();
						       
							  Product product1 = new Product(nameProduct,quantityProduct,priceProduct);

						        try {
						           clientBLL.insert(product1);
						        } catch (Exception e){
						           // LOGGER.log(Level.INFO, e.getMessage());

						        }


						        if(product1!=null){
						            System.out.println(product1.toString());
						       } 

						 }
					 
					 
						 else
							 
						 {
							 if(rez[0].equals("Delete product")) {
								 String [] detailsProduct;
								 detailsProduct=rez[1].split(", ");
								 nameProduct=detailsProduct[0];
								 //function of delete product of DB
								 ProductBLL productBLL = new ProductBLL();
							       
									boolean ok1=false;

							        try {
							          ok1= productBLL.deleteProductByName(nameProduct);
							        } catch (Exception e){
							        }

							 }
					 
							 else
							 {
								 if(rez[0].equals("Order")) {
									 String [] detailsOrder;
									 detailsOrder=rez[1].split(", ");
									 nameClient = detailsOrder[0];
									 nameClient = nameClient.substring(1,nameClient.length() );
									 nameProduct=detailsOrder[1];
									 quantityProduct=Integer.parseInt(detailsOrder[2]);
									


									 //function for order
									 
										//OrderBLL orderBLL = new OrderBLL();							 
										//orderBLL.insertOrder(nameClient,nameProduct,quantityProduct);	
									 
									


								 }
					
						
				}
			 }
		}
	}
}
				else
					{
					
					  if(citeste.equals("Report client"))
					  {
						  ClientBLL clientBLL = new ClientBLL();
					       
							List<Client> listClients = clientBLL.findAllClients();		
							
							 if(listClients!=null){
						            System.out.println(listClients.toString());
						        }
						  
						  GeneratePDF pdf =new GeneratePDF();
						  pdf.generateClientReport(listClients);
					  }
					  else
					  {
						  if(citeste.equals("Report product"))
						  {
							  //generare PDFProduct;
							  ProductBLL productBLL = new ProductBLL();
						       
								List<Product> listProducts = productBLL.findAllProducts();		
								
								 if(listProducts!=null){
							            System.out.println(listProducts.toString());
							        }
							  
							  GeneratePDF pdf =new GeneratePDF();
							  pdf.generateProductsReport(listProducts);
						  }
			         
			}
		}
				System.out.println(citeste);
				citeste=buf.readLine();
				//System.out.println(citeste);
			}
		 
		 
	   
	 }
	
}
