package Model;

public class Product {

	private int id;
	private String name;
	private int initialQuantity;
	private float price;
	
	
	
		
	public Product() {
		super();
	}


	
	public Product( String name, int initialQuantity,float price) {
	
		this.name = name;
		this.initialQuantity = initialQuantity;
		this.price = price;
	}
	
	
	

	public Product(int id, String name, int initialQuantity, float price) {
		super();
		this.id = id;
		this.name = name;
		this.initialQuantity = initialQuantity;
		this.price = price;
	}

	
	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public float getPrice() {
		return price;
	}



	public void setPrice(float price) {
		this.price = price;
	}



	public int getInitialQuantity() {
		return initialQuantity;
	}



	public void setInitialQuantity(int initialQuantity) {
		this.initialQuantity = initialQuantity;
	}



	@Override
	public String toString() {
		return "Product [idProduct=" + id + ", name=" + name + ", price=" + price + ", initialQuantity=" + initialQuantity +  "]";
	}
	
}
