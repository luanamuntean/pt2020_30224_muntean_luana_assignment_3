package Model;

public class Client {

	private int id;
	private String name;
	private String town;
	private String email;
	private int age;
		
	
	public Client() {
		
	}


	
public Client(String name, String town) {
		
		this.name = name;
		this.town = town;
		
	}
	

	
	
	public Client( String name, String town, String email, int age) {
		
		this.name = name;
		this.town = town;
		this.email = email;
		this.age = age;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id= id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getTown() {
		return town;
	}


	public void setTown(String town) {
		this.town = town;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}
	
	
	
	@Override
	public String toString() {
		return "Client [idClient=" + id + ", name=" + name + ", town=" + town + ", email=" + email + ", age=" + age
				+ "]";
	}
	
	
}
