package Bll.validator;

/**
 * Interfata pentru validarea parametrilor  
 * 
 * @author user
 *
 * @param <T>
 *            clasa obiectului pentru care se face validarea
 */


public interface Validator<T> {
    public void validate(T t);
}
