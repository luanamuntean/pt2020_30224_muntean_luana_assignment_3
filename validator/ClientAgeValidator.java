package Bll.validator;

import Model.Client;

/**
 * Aceasta clasa verifica daca varsta unui client se afla in intervalul impus 
 *  
 * @author user
 */

public class ClientAgeValidator implements Validator<Client> {
	private static final int MIN_AGE = 15;
	private static final int MAX_AGE = 65;

	public void validate(Client t) {

		if (t.getAge() < MIN_AGE || t.getAge() > MAX_AGE) {
			throw new IllegalArgumentException("The Student Age limit is not respected!");
		}

	}

}