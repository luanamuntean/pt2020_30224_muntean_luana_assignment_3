package Bll.validator;

import Bll.ProductBLL;


import Model.Orders;
import Model.Product;


/**
 * Aceasta clasa verifica daca produsul dorit de client exista sau nu in baza de date 
 * 
 * @author user
 */

public class OrderProductValidator implements Validator<Orders> {

	@Override
	public void validate(Orders t) {
		// TODO Auto-generated method stub
		ProductBLL productBLL=new ProductBLL();
			Product product =productBLL.findProductById(t.getIdProduct());
			if(product==null) {
				throw new IllegalArgumentException("Product with id = " + t.getIdProduct() + " was not found!");
			}
			else
			{
				System.out.println("Order taken!");
			}

	
	}

}
